# Study Springboot

스프링 부트와 관련된 토이 프로젝트 진행.

# IDE를 구분하여 샘플 프로젝트 진행.

- eclipse sts4 IDE 기반 샘플 프로젝트 진행.(해당 프로젝트를 임포트 하여 프로젝트 실행)
- intelj 기반의 spring boot 샘플 프로젝트 진행.

# 기능 구현의 목적.
- Debug app log 출력, access log 출력.
- REST API 구현
- DB 를 만들어서 간단한 게시판 구현진행.
