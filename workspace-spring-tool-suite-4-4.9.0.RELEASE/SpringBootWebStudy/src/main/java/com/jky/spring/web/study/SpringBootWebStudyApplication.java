package com.jky.spring.web.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebStudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebStudyApplication.class, args);
	}

}
